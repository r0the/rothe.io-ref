# :mdi-cursor-default-click: guizero
---

Mit der Python-Bibliothek guizero können auf einfache Weise Anwendungen mit grafischer Benutzeroberfläche erstellt werden.

## Import-Anweisung

Um die guizero-Bibliothek zu verwenden, muss die folgende Import-Anweisung verwendet werden:

``` python
from guizero import *
```

## Links

* [:link: Webseite](https://lawsie.github.io/guizero/)