from guizero import *

fenster = App(
    title = "Beispiel Taste",
    width = 640,
    height = 320
)

def befehl():
    fenster.yesno("Info", "Taste ist geklickt worden.")

taste = PushButton(fenster, command = befehl)
taste.text = "Klick mich"

fenster.display()
