# Elemente
---

## Text

Ein Text wird mit der folgenden Anweisung erzeugt:

~~~ python
text = Text(fenster, text="Beschriftung")
~~~

![](./text.png)

## Schieber

Ein Schieber wird mit der folgenden Anweisung erzeugt:

~~~ python
schieber = Slider(fenster, start=minimum, end=maximum)
~~~

Dabei kann mit `start` der minimale Wert und mit `end` der maximale Wert angegeben werden. Um einen vertiklaen Schieber zu erhalten, ergänzt man mit `horizontal=False`.

![](./slider.png)

Mit `schieber.value` kann der aktuelle Wert ausgelesen oder gesetzt werden.

## Taste

Eine Taste wird mit der folgenden Anweisung erstellt:

~~~ python
taste = Slider(fenster, text="Beschriftung", command=unterprogramm)
~~~

![](./pushbutton.png)

## Eingabefeld

Ein Eingabefeld wird mit der folgenden Anweisung erstellt:

~~~ python
eingabefeld = TextBox(fenster)
~~~

Mit `eingabefeld.value` kann der aktuelle Inhalt des Feldes ausgelesen oder gesetzt werden. Ausserdem können bei der `TextBox`-Anweisung folgende Parameter angegeben werden:

| Parameter   | Typ           | Bedeutung                           |
|:----------- |:------------- |:----------------------------------- |
| `width`     | Zahl          | Anzahl sichtbare Zeichen            |
| `height`    | Zahl          | Anzahl Zeilen (nur mit `multiline`) |
| `multiline` | Wahrheitswert | mehrzeiliges Eingabefeld            |
| `hide_text` | Wahrheitswert | Zeichen verstecken (Passwort)       |

![](./textbox.png)

## Picture

Ein Bild kann mit der folgenden Anweisung dargestellt werden:

~~~ python
bild = Picture(fenster, image="bild.jpg")
~~~

![](./picture.png)