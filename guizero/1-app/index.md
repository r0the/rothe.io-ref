# 1 Anwendung
---

## Erzeugung

~~~ python
fenster = App(title="guizero", width=500, height=500)
~~~
erstellt ein Anwendungsfenster. Mit `title=` kann der Titel des Fensters festgelegt werden. Mit `width=` und `height=` werden Breite und Höhe des Fensters in Pixel festgelegt.

## Befehle

~~~ python
fenster.display()
~~~
startet die Anwendung. Dies muss die **letzte** Anweisung im Programm sein.

~~~
fenster.set_full_screen()
~~~
zeigt das Fenster im Vollbildmodus an. Mit [Esc] wird dieser beendet.

~~~
fenster.exit_full_screen()
~~~
beendet den Vollbildmodus.


## Beispiel

``` python
from guizero import *

fenster = App(title="Hallo Welt")
fenster.display()
```

