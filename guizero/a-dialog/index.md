# Dialoge
---

## Information anzeigen

~~~ python
fenster.info(titel, text)
~~~

~~~ python
fenster.warn(titel, text)
~~~


## Ja/Nein-Frage

~~~ python
if (fenster.yesno(titel, text)):
    # Ja angeklickt
else:
    # Nein angeklickt
~~~

## Farbe auswählen

~~~ python
farbe = fenster.select_color()
~~~

## Datei auswählen

~~~ python
datei = fenster.select_file()
~~~

## Ordner auswählen

~~~ python
ordner = fenster.select_folder()
~~~

