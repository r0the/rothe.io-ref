# Aussehen
---

Das Aussehen aller Elemente kann mit folgenden Eigenschaften geändert werden:

| Eigenschaft  | Typ           | Bedeutung                  |
|:------------ |:------------- |:-------------------------- |
| `bg`         | Farbe         | Hintergrundfarbe           |
| `enabled`    | Wahrheitswert | Ist das Element aktiviert? |
| `font`       | Text          | Schriftart                 |
| `height`     | Grösse        | Höhe des Elements          |
| `text_size`  | Zahl          | Schriftgrösse              |
| `text_color` | Farbe         | Textfarbe                  |
| `visible`    | Wahrheitswert | Ist das Element sichtbar?  |
| `width`      | Grösse        | Breite des Elements        |

## Farbe

Eine Farbe wird als RGB-Wert angegeben, z.B. (255, 0, 0) für Rot.

## Grösse

Eine Grösse ist entweder eine Angabe in Pixel oder der Wert `"fill"`.

