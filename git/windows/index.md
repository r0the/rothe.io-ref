# Installation Windows
---

## 1. Client installieren

Lade die App **Sourcetree** von folgender Webseite herunter:

* [:link: Sourcetree](https://www.sourcetreeapp.com)

1. Starte das Installationsprogramm und klicke hier auf _Überspringen_:

    ![](./sourcetree-install-1.png)

2. Entferne den Haken bei _Mercurial_ und klicke auf _Weiter_:

    ![](./sourcetree-install-2.png)

3. Warte bis alle fertig installiert worden ist:

    ![](./sourcetree-install-3.png)

4. Gib deinen Namen und deine E-Mail-Adresse ein. Damit werden die Git-Commits, welche du machst, gekennzeichnet.

    ![](./sourcetree-install-4.png)

5. Klicke hier auf _Nein_:

    ![](./sourcetree-install-5.png)

## 2. SSH-Schlüssel erstellen

Git verwendet SSH (Secure Shell), um mit dem Server zu kommunizieren. Damit du dich per SSH am Git-Server anmelden kannst, musst du einen **SSH-Schlüssel** erstellen.

1. Starte Sourcetree und wähle im Menü _Tools_ den Menüpunkt _SSH-Schlüssel importieren oder erstellen_ aus. Damit wird das Hilfsprogramm PuTTY Key Generator gestartet.

2. Wähle unten bei _Type of key to generate_ die Variante _ED25519_ aus und klicke auf _Generate_:

    ![](./putty-keygen-1.png)

2. Nun musst du die Maus über dem Fenster bewegen, um Zufall für den Schlüssel zu erzeugen. Speicher anschliessend den **privaten Schlüssel**, indem du auf _Save private key_ klickst.

    ![](./putty-keygen-2.png)

3. Wähle den öffentlichen Schlüssel (im Beispiel blau markiert) im Fenster aus und kopiere ihn in die Zwischenablage.

4. Gehe auf die GitLab-Webseite ins oben rechts in Menü _Settings_ und wähle anschliessend links den Menüeintrag _SSH Keys_ aus. Füge den öffentlichen Schlüssel in das entsprechende Feld, schreibe in das Feld _Title_ eine Bezeichnung (z.B. «Sourcetree») und klicke auf _Add key_:

    ![](./gitlab-ssh-key.png)

## 3. SSH-Schlüssel verwenden

Damit der SSH-Schlüssel von Git verwendet werden kann, muss das Hilfsprogramm **Pageant** gestartet werden. Das geschieht ebenfalls aus Sourcetree im Menü _Tools_ mit dem Menüpunkt _Starte SSH-Agent_.

Der im vorderen Schritt erzeugte und gespeicherte Schlüssel muss nun in diesem Hilfsprogramm geladen werden.

1. Öffne das Hilfsprogramm, indem du im Menübalken auf den nach oben zeigenden Winkel und anschliessend auf das Computer-Symbol klickst:

    ![](./pageant-1.png)

2. Klicke auf _Add Key_ und wähle die Datei, in welchen du den privaten Schlüssel gespeichert hast.

    ![](./pageant-2.png)

3. Nun ist der Schlüssel für Git verfügbar, das Fenster kann geschlossen werden:

    ![](./pageant-3.png)
