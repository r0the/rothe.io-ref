# :mdi-git: Git
---

Git ist ein bekanntes Versionsverwaltungssystem.

## Dienste

An unserer Schule betreiben wir einen GitLab-Server:

* [:link: GitLab Gymnasium Kirchenfeld](https://gitlab.gymkirchenfeld.ch)

Ausserdem gibt es bekannte öffentliche Git-Server, welche gratis verwendet werden können:

* [:link: GitHub](https://github.com/)
* [:link: GitLab](https://gitlab.com/)

## Clients

* [:link: Sourcetree](https://sourcetreeapp.com)
