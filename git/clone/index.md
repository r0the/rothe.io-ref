# Klonen
---

Durch das Klonen wird auf dem lokalen Computer eine Kopie der Repositorys angelegt. Dazu muss die SSH/Adresse des Git-Repositorys bekannt sein.

## SSH-Adresse auf GitLab

Gehe auf die GitLab-Webseite des gewünschten Repositorys, klicke auf _Clone_ und kopiere die Adresse unter _Clone with SSH_:

![](./gitlab-clone.png)

## Klonen mit Sourcetree

1. Öffne Sourcetree, klicke auf _Clone_ und füge die SSH-Adresse in das oberste Feld ein.

2. Wähle im zweiten Eingabefeld den Ordner, in welchen du das Repository kopieren möchtest und klicke auf _Klone_:

    ![](./sourcetree-clone.png)
