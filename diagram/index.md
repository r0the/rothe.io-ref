# :mdi-sitemap: Diagramme
---

Nachschlagewerk für verschiedene Diagrammtypen und Darstellungen, welche in der Informatik verwendet werden.

* [Flussdiagramm](?page=flowchart/)
* [Zustandsdiagramm](?page=statechart/)
* [Concept-Map](?page=concept-map/)
