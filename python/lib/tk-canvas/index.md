# Tkinter Canvas
---

## Tk-Anwendung
Um eine Anwendung mit Grafik zu programmieren, muss das Modul `tkinter` importiert werden:

``` python
import tkinter as tk
```

Anschliessend wird ein Anwendungsobjekt erzeugt:

``` python
app = tk.Tk()
```

Das Anwendungsobjekt muss allen Elementen der Benutzeroberfläche als Parameter übergeben werden, beispielsweise

``` python
label = tk.Label(app, text="Hello World")
label.pack()
```

Am Ende der Initialisierung muss der Event-Loop der Anwendung gestartet werden:

``` python
app.mainloop()
```

Ein vollständiges Beispiel:

``` python
import tkinter as tk

app = tk.Tk()
label = tk.Label(app, text="Hello World")
label.pack()
app.mainloop()
```

## Canvas

``` python
import tkinter as tk

app = Tk()
canvas = Canvas(app, bg="blue", width=800, height=600)
canvas.pack()
app.mainloop()
```
