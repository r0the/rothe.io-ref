# Module
---

Hier werden wichtige Module vorgestellt:

* [Mathematische Funktionen mit `math`](?page=math/)
* [Zufall mit `random`](?page=random/)
* [Turtlegrafik mit `turtle`](?page=turtle/)
