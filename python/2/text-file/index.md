# Textdateien
---

## Datei öffnen und schliessen

~~~ python
with open(<mark>dateiname</mark>, mode="r", encoding="utf-8") as datei:
~~~
öffnet die Textdatei mit dem Dateinamen `dateiname` und führt die folgenden, eingerückten Anweisungen mit dem Datei-Objekt `datei` aus. Mit `mode=` wird festgelegt, wie auf die Textdatei zugegriffen werden soll:

| Modus | Bedeutung                                                               |
|:----- |:----------------------------------------------------------------------- |
| `r`   | Datei wird gelesen (Standard)                                           |
| `w`   | Datei wird neu erstellt oder überschrieben                              |
| `x`   | Datei wird neu erstellt, eine bestehende Datei aber nicht überschrieben |
| `a`   | Daten werden an die Datei angehängt                                     |
| `b`   | Zugriff auf eine binäre Datei                                           |

Mit `encoding=` wird die Zeichencodierung der Textdatei angegeben. Diese Angabe sollte unbedingt vorhanden sein, da das Standardverhalten je nach Betriebssystem unterschiedlich ist.

## Datei lesen

~~~ python
<mark>datei</mark>.read()
~~~
liest den ganzen Inhalt einer Datei und liefert ihn zurück.

``` python
with open("test.txt", encoding="utf-8") as datei:
    inhalt = datei.read()
```

~~~ python
<mark>datei</mark>.readlines()
~~~
liest den ganzen Inhalt einer Datei zeilenweise und liefert ihn als Liste aller Zeilen zurück.

``` python
with open("test.txt", encoding="utf-8") as datei:
    zeilen = datei.readlines()
```

~~~ python
for <mark>zeile</mark> in <mark>datei</mark>:
~~~
liest eine Textdatei zeilenweise.

``` python
with open("test.txt", encoding="utf-8") as datei:
    for zeile in datei:
        print(zeile)
```

## Datei schreiben

~~~ python
<mark>datei</mark>.write(<mark>text</mark>)
~~~
schreibt `text` in die geöffnete Datei `datei`.

``` python
with open("test.txt", mode="w", encoding="utf-8") as datei:
    datei.write("Hello\nWorld")
```
