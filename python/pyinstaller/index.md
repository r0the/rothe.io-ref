# Pyinstaller
---

Mit Pyinstaller kann aus Python-Programmen installierbare Apps gemacht werden.

## Installation auf Windows

Zuerst muss Python 3.9 installiert werden:

* [:download: Python 3.9.1 für Windows](https://www.python.org/ftp/python/3.9.1/python-3.9.1-amd64.exe)

Anschliessend wird die Kommandozeile geöffnet und die folgende Befehle ausgeführt:

1. PIP aktualisieren
    ```
    py -m pip install --upgrade pip
    ```

2. wheel installieren

    ```
    py -m pip install wheel
    ```
3. pypiwin32 installieren

    ```
    py -m pip install pypiwin32
    ```

4. pyinstaller Version 4.1 installieren (4.2 funktioniert nicht)

    ```
    py -m pip install pyinstaller==4.1
    ```
