# :mdi-language-python: Python-Referenz
---

## Sprachumfang GYM1

::: columns 3
* [Kommentare](?page=1/comment/)
* [Namen](?page=1/name/)
* [Werte und Typen](?page=1/type/)
* [Anweisungen](?page=1/statement/)
***
* [Wahrheitswerte](?page=1/boolean/)
* [Zahlen und Rechnen](?page=1/number/)
* [Zeichenketten](?page=1/string/)
***
* [Kontrollstrukturen](?page=1/control/)
* [Unterprogramme](?page=1/subroutine/)
* [Import von Modulen](?page=1/import/)
:::

## Sprachumfang GYM2

* [Textdateien](?page=2/text-file/)
* [Listen]


## Sprachumfang GYM3

* [Klassen definieren](?page=3/class/)
* [Operatoren definieren](?page=3/operator/)
* [Wörterbücher]

## Module


## Links

* [:link: Python-Tutorial](https://py-tutorial-de.readthedocs.io/)
* [:link: Advent of Code](https://adventofcode.com/)
