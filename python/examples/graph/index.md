# Graphen zeichnen
---

Mit Hilfe des `turtle`-Moduls können einfach Funktionsgraphen gezeichnet werden:

``` python turtle_graph.py
```
