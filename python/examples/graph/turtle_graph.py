from turtle import *
from math import sin


FONT = ("Arial", 16, "normal")

x_min = -10
y_min = -10
x_max = 10
y_max = 10

def zeichne_achsen():
    # x-Achse zeichnen
    penup()
    goto(x_min, 0)
    write(x_min, False, align="left", font=FONT)
    pendown()
    goto(x_max, 0)
    write(x_max, False, align="right", font=FONT)
    # y-Achse zeichnen
    penup()
    goto(0, y_min)
    write(y_min, False, align="right", font=FONT)
    pendown()
    goto(0, y_max)
    penup()
    write(y_max, False, align="right", font=FONT)


def zeichne_graph(funktion):
    penup()
    schritt = (x_max - x_min) / 100
    x = x_min
    while x < x_max:
        y = funktion(x)
        goto(x, y)
        pendown()
        x = x + schritt


def linear(x):
    return 0.5 * x


def sinus(x):
    return 2 * sin(x)


def parabel(x):
    return 0.1 * x ** 2 - 5


def main():
    # Koordinatensystem festlegen
    setworldcoordinates(x_min, y_min, x_max, y_max)
    # Turtle verstecken, muss nach setworldcoordinates aufgerufen werden
    hideturtle()
    # Zeichnen ohne Turtle-Animation
    tracer(0)
    
    zeichne_achsen()
    color("red")
    zeichne_graph(linear)
    color("blue")
    zeichne_graph(sinus)
    color("green")
    zeichne_graph(parabel)
    
    # Wenn ohne Animation gezeichnet wird, muss mit update() die Grafik explizit
    # angezeigt werden.
    update()

main()
