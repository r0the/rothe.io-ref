# Sprache 1
---

Die folgenden Sprachelemente werden ab Stufe GYM1 behandelt:

* [Kommentare](?page=comment/)
* [Namen](?page=name/)
* [Werte und Typen](?page=type/)
* [Anweisungen](?page=statement/)
* [Wahrheitswerte](?page=boolean/)
* [Zahlen und Rechnen](?page=number/)
* [Zeichenketten](?page=string/)
* [Kontrollstrukturen](?page=control/)
* [Unterprogramme](?page=subroutine/)
* [Import von Modulen](?page=import/)
