# Zahlen und Rechnen
---

## Zahlentypen

Python kennt zwei Arten von Zahlen:

- **Ganze Zahlen** haben den Typ `int`. Sie haben im Gegensatz zu den meisten Programmiersprachen keine Beschränkung und können beliebig gross werden.

- **Gleitkommazahlen** haben in Python den Typ `float`. Sie werden von Computern verwendet, um rationale und reelle Zahlen annähernd darzustellen. Technisch gesehen sind sie Binärzahlen mit Nachkommastellen, ihr Verhalten ist deshalb manchmal ungewöhnlich. Zum beseren Verständnis wird [diese Erklärung](https://docs.python.org/3/tutorial/floatingpoint.html) empfohlen. Die Limiten von Gleitkommazahlen hängen von der Hardware und dem Betriebssystem ab, auf welchem Python ausgeführt wird. Es kann üblicherweise davon ausgegangen werden, dass der [IEEE-Gleitkommazahlentyp](https://de.wikipedia.org/wiki/IEEE_754)
double verwendet wird und somit Zahlen bis zu ca. $\pm 10^{\pm308}$ dargestellt werden können.

Ausserdem bietet Python unterstützung für die exakte Darstellung von rationalen Zahlen als Brüche mit dem Modul [`fractions`](https://docs.python.org/3.5/library/fractions.html) und von Dezimalzahlen mit dem Modul [`decimal`](https://docs.python.org/3.5/library/decimal.html).

## Schreibweise

Natürliche Zahlen (Typ `int`) bestehen aus einer Folge von Ziffern. Dabei darf die erste Ziffer nicht die `0` sein, ausser bei der Zahl $0$.

``` python
7    3    2147483647    79228162514264337593543950336
```

Wenn eine Zahl ein Dezimalpunkt `.` enthält, gilt sie als Gleitkommazahl (Type `float`). Diese können auch in Exponentialschreibweise dargestellt werden, dabei wird der Buchstabe `e` als Platzhalter für «mal Zehn hoch» verwendet. So wird die Zahl $6.674\cdot 10^{-11}$ in Python als `6.674e-10` geschrieben.

``` python
3.14    10.    .001    1e100    3.14e-10    0e0
```

::: info Negative Zahlen

In Programmiersprachen können negative Zahlen nicht direkt im Code dargestellt werden, sondern als Negation einer positiven Zahl. Wenn in Python `-2` geschrieben wird, wird dies als die Zahl `2` interpretiert, auf welche eine Negation angewendet wird.
:::

## Operationen

| Operation            | Mathematisch                            | Python                      |
|:-------------------- |:--------------------------------------- | ---------------------------:|
| Addition             | $a + b$                                 | `a + b`                     |
| Subtraktion          | $a - b$                                 | `a - b`                     |
| Negation             | $-a$                                    | `-a`                        |
| Multiplikation       | $a \cdot b$                             | `a * b`                     |
| Division             | $\frac{a}{b}$                           | `a / b`                     |
| Potenz               | $a^b$                                   | `a ** b`                    |
| ganzzahlige Division | $\lfloor\frac{a}{b}\rfloor$             | `a // b`                    |
| Modulo               | $a - \lfloor\frac{a}{b}\rfloor \cdot b$ | `a % b`                     |
| Division mit Rest    | $a \div b = c \;\text{Rest}\; r$        | `c = a // b`<br>`r = a % b` |

Python unterstützt die Grundoperationen mit Zahlen: Additition, Subtraktion, Multiplikation und Division. Das Resultat einer Division ist immer eine Gleitkommazahl.

``` python
2 + 3   # ergibt 5
(50 - 5*6) / 4   # ergibt 5.0
```

Ausserdem kennt Python die Potenz-Operation und die Negation:

``` python
-2**10  # ergibt -1024
(4**2 + 3**2)**0.5   # ergibt 5.0
```

Für die Operationen gilt die übliche Rangfolge, welche wie üblich durch Klammern verändert werden kann. Der (Nicht-)Gebrauch von Leerzeichen um Operationszeichen hat keinen Einfluss auf die Rangfolge, kann jedoch verwendet werden, um die Rangfolge zu betonen:

``` python
2 * 3+1   # ergibt 7, schlechter Programmierstil!
```

Schliesslich kennt Python die Division mit Rest. Da eine Operation nur eine Zahl als Resultat liefern kann, wird in Programmiersprachen die Division mit Rest üblicherweise in zwei Operationen aufgeteilt: Eine ganzzahlige Division und die Modulo-Operation. Die ganzzahlige Division berechnet die nächste ganze Zahl zu $\frac{a}{b}$ in Richtung $0$, der Modulo berechnet den Rest der Division $\frac{a}{b}$ und liegt immer zwischen $0$ und $b-1$.

``` python
# 7 durch 3 = 2 Rest 1
7 // 3   # ergibt 2
7 % 3    # ergibt 1
```

## Eingebaute Funktionen

### Umwandlung von Zahlentypen

~~~ python
int(x)
~~~
konvertiert den Text oder die Zahl `x` in eine ganze Zahl. Ob eine Fliesskommzahl auf- oder abgerundet wird, ist nicht definiert.

~~~ python
float(a)
~~~
konvertiert den Text oder die Zahl `x` in eine Gleitkommazahl.

### Mathematische Funktionen

~~~ python
abs(x)
~~~
liefert den Betrag von `x` zurück.

~~~ python
max(x, y, z, ...)
~~~
liefert das Maximum der angegebenen Werte.

~~~ python
min(x, y, z, ...)
~~~
liefert das Minimum der angegebenen Werte.

~~~ python
round(x, d)
~~~
rundet die Fliesskommazahl `x` auf `d` Nachkommastellen.

**Achtung:** Python 3 rundet nach dem Schema: «round half to even»!
