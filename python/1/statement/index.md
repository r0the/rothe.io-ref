# Anweisung
---

Anweisungen sind die «Sätze» eines Programms oder Skripts. In Python steht jede Anweisung auf einer separaten Zeile. Es werden verschiedene Arten von Anweisungen unterschieden:

- Wert zuweisen `=`
- Modul importieren `import`
- Unterprogramm definieren `def`
- Unterprogramm aufrufen `()`
- Verzweigung / bedingte Ausführung `if`
- Iteration `for`
- Schleife `while`

Jede Anweisung in Python kann einem Punkt der obenstehenden Liste zugeordnet werden.

## Zuweisung

Mit einer Zuweisung wird einer Variable ein Wert zugeordnet. Für die Zuweisung wird in Python das Gleichheitszeichen verwendet `=`.

``` python
WIDTH = 800
```
weist der Variable mit dem Namen `WIDTH` den Wert `800` zu. Die Zuweisung darf nicht mit einer mathematischen Gleichheit verwechselt werden. Es ist treffender, sich eine Bewegung von rechts nach links vorzustellen. Der Wert rechts des Gleichheitszeichens wird in der Variable links davon gespeichert:

![](images/assignment.svg)

Die Zuweisung darf nicht mit einer mathematischen Gleichheit verwechselt werden. Bei der Zuweisung wird in der Variable links vom Gleichheitszeichen der Wert rechts davon gespeichert.

Ein gespeicherter Wert kann jederzeit durch eine erneute Zuweisung überschrieben werden. Deshalb sollten Konstanten in Grossbuchstaben geschrieben, um darauf hinzuweisen, dass sie nicht verändert werden sollten.
