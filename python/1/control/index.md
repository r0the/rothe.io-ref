# Kontrollstrukturen
---

Normalerweise werden die Anweisungen eines Python-Programms zeilenweise von oben nach unten abgearbeitet. Mit sogenannten **Kontrollstrukturen** kann diese Reihenfolge beeinflusst werden.

## Bedingte Anweisung

Wenn eine oder mehrere Anweisungen nur manchmal ausgeführt werden sollen, kann eine **bedingte Anweisung** verwendet werden. Eine solche Anweisung beginnt mit dem Schlüsselwort `if`, gefolgt von einer Bedingung und einem Doppelpunkt. Auf den nächsten Zeilen folgen die Anweisung(en), welche nur ausgeführt werden sollen, wenn die Bedingung erfüllt ist. Diese müssen gegenüber der Bedingung eingerückt werden.

``` python
if alter >= 65:
    # Wird nur ausgeführt, wenn alter >= 65
    print('Sie sind im Ruhestand.')
# Wird immer ausgeführt
print('Sie sind ' + str(alter) + ' Jahre alt.')
```

## Alternative Anweisungen

``` python
if alter >= 65:
    # Wird nur ausgeführt, wenn alter >= 65
    print('Sie sind im Ruhestand.')
else:
    # Wird nur ausgeführt, wenn alter < 65
    print('Sie sind nicht im Ruhestand')
# Wird immer ausgeführt
print('Sie sind ' + str(alter) + ' Jahre alt.')
```
``` python
if alter >= 65:
     # Wird nur ausgeführt, wenn 65 <= alter
    print('Sie sind im Ruhestand.')
elif alter >= 18:
    # Wird nur ausgeführt, wenn 18 <= alter < 65
    print('Sie sind erwerbstätig.')
else:
    # Wird nur ausgeführt, wenn alter < 18
    print('Du bist noch nicht volljährig.')
# Wird immer ausgeführt
print('Sie sind ' + str(alter) + ' Jahre alt.')
```

## Iteration einer Sequenz

``` python
for i in range(1, 11):
    s = 'Das Quadrat von ' + str(i) + ' ist ' + str(i*i) + '.'
    print(s)
```

## Vorprüfende Schleife

Mit einer vorprüfenden Schleife können Anweisungen solange wiederholt werden, bis eine Bedingung eintritt. Die Struktur einer solchen Schleife ist analog zur Struktur einer bedingten Anweisung, das Schlüsselwort `if` wird jedoch durch `while` ersetzt.

``` python
zahl = 42
geraten = 0
while geraten != zahl:
    geraten = int(input('Errate die Zahl:'))
print('Richtig!')
```

Eine solche Schleife wird typischerweise verwendet, wenn das Programm auf das Eintreten einer externen Bedingung wartet, beispielsweise auf eine bestimmte Eingabe des Benutzers.
