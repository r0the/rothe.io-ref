# Komplexe Zahlen
---

Python kann auch mit komplexen Zahlen umgehen. Die imaginäre Einheit $i$ wird in Pyzhon mit dem Buchstaben `j` dargestellt. So wird in Python die komplexe Zahl `3 + 4i` so geschrieben:

``` python
3 + 4j
```

## Funktionen

~~~ python
complex(<mark>x</mark>, <mark>y</mark>)
~~~
liefert die komplexe Zahl $x+yi$ zurück.

~~~ python
<mark>z</mark>.real
~~~
liefert den rellen Teil der komplexen Zahl `z` zurück.

~~~ python
<mark>z</mark>.imag
~~~
liefert den imaginären Teil der komplexen Zahl `z` zurück.

## Beispiel

``` python
z = complex(3, 4)
print("komplexe Zahl z:", z)
print("reeller Teil von z:", z.real)
print("imaginärer Teil von z:", z.imag)
```
