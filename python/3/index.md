# Sprache 3
---

Die folgenden Sprachelemente werden ab Stufe GYM3 behandelt:

* [Klassen definieren](?page=class/)
* [Operatoren definieren](?page=operator/)
* [komplexe Zahlen](?page=complex/)
