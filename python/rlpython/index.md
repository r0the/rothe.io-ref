# Real Life Python
---

Python ist einer der beliebtesten Programmiersprachen der Welt. Sie wird besonders im wissenschaftlichen Bereich eingesetzt.

Hier ein einige Beispiele von Bibliotheken und Anwendungen für Python:

::: cards 2
[![](./nltk.png)][1]

#### [NLTK][1]
Mit dem Natural Language Toolkit können in Python natürliche Sprachen analysiert und verarbeitet werden.

***
[![](./matplotlib.png)][2]
#### [Matplotlib][2]
Mit der Matplotlib können Daten als Funktionen, Diagramme usw. visualisiert werden.

***
[![](./numpy.png)][3]
#### [Numpy][3]
Numpy ist eine grundlegende Bibliothek für numerische Mathematik (Vektor- und Matrixrechnung). Sie wird von vielen anderen Bibliotheken verwendet und wurde beispielsweise für den Nachweis der Gravitationswellen eingesetzt.

***
[![](./tensorflow.png)][4]
#### [Tensorflow][4]
TensorFlow ist eine weitverbreitete Plattform für maschinelles Lernen auf Basis von künstlichen neuronalen Netzwerken.

***
[![](./biopython.svg)][5]
#### [Biopython][5]
Biopython ist eine Sammlung von Tools und Bibliotheken in Python für die computergestützte Molekularbiologie.

***
[![](./blender.png)][6]
#### [Blender][6]
Die 3D-Engine Blender, mit welcher professionelle Animationsfilme erstellt werden, kann mit Python programmiert und erweitert werden.
:::

[1]: https://www.nltk.org/
[2]: https://matplotlib.org/
[3]: https://numpy.org/
[4]: https://www.tensorflow.org/
[5]: https://biopython.org/
[6]: https://www.blender.org/
