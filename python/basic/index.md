# Basale Kompetenzen
---

Hier werden die zehn häufigsten Fehler in Python zusammengefasst.

## 1. Rechtschreibung von Namen

Namen müssen korrekt geschrieben werden. Eine sehr häufige Fehlerursache ist, dass Namen falsch geschrieben werden:

- Die Gross- und Kleinschreibung von Namen ist relevant. Die Namen `WERT`, `Wert` und `wert` beziehen sich auf unterschiedliche Variablen.
- Vordefinierte Namen verwenden meist die amerikanische Schreibweise. So lautet ein Turtlebefehl `setcolor`, nicht `setcolour`.

## 2. Gleichheit und Zuweisung

Eine Zuweisung «speichert» einen Wert in einer Variable:
``` python
x = 5
```

Ein Vergleich überprüft, ob zwei Werte gleich sind:
``` python
x == 5
```

Ein Vergleich wird normalerweise in einer Kontrollstruktur verwendet:
``` python
if x == 5:
```

## 3. Zählen beginnt bei Null

Das Zählen beginne immer bei Null. Beispiele:

- Das folgende Programm gibt die Zahlen `0` bis `9` aus:
    ``` python
    for i in range(10):
        print(i)
    ```

- Das erste Zeichen einer Zeichenkette `s` erhält man mit `s[0]`.

- Das erste Element einer Liste `l` erhält man mit `l[0]`.

## 4. Einrückung

## 5. Unterprogramm aufrufen

Damit ein Unterprogramm aufgerufen wird, müssen die Klammern `()` geschrieben werden. Wenn man die Klammern weglässt, ist dies immer noch korrektes Python:

``` python
turtle.penup # korrekter Code, aber kein Aufruf des Unterprogramms
turtle.penup() # penup wird aufgerufen
```

## 6. Modul importieren

## 7. Zeichenketten in Anführungszeichen
