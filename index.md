# :mdi-bookshelf: Referenz
---

::: cards 3
<v-book name="thonny"/>

***
<v-book name="python-ref"/>

***
<v-book name="microbit-ref"/>

***
<v-book name="pgzero-ref"/>

***
<v-book name="guizero-ref"/>

***
<v-book name="git"/>

***
<v-book name="diagram"/>

***
<v-book name="electronics"/>

:::
