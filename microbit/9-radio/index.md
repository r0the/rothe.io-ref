# Funk
---

Für den Zugriff auf den Funkchip muss zusätzlich das Modul `radio` importiert werden:
``` python
import radio
```

Anschliessend können die folgenden Anweisungen verwendet werden:

~~~ python
radio.on()
~~~
schaltet den Funkchip ein.

~~~ python
radio.off()
~~~
schaltet den Funkchip aus.

~~~ python
radio.config(length=<mark>32</mark>, address=<mark>1969383796</mark>, group=<mark>0</mark>)
~~~
konfiguriert den Funkchip.

~~~ python
radio.reset()
~~~
setzt den Funkchip auf die Standardeinstellungen zurück. Alle durch `radio.config()` vorgenommenen Einstellungen werden zurückgesetzt.

~~~ python
radio.send(<mark>nachricht</mark>)
~~~
sendet den Text `nachricht`. Beispiel:

``` python
import radio

radio.on()
radio.send("Hallo Welt!")
```

~~~ python
radio.receive()
~~~
liefert den zuletzt empfangenen Text zurück. Falls kein empfangener Text vorhanden ist, wird `None` zurückgeliefert.
