# Bilder
---

Mit `Image` werden Bilder definiert, welche auf dem Display angezeigt werden können. Die folgenden vordefinierten Bilder sind vorhanden:

|                  |                       |                        |
|:---------------- |:--------------------- |:---------------------- |
| `Image.CLOCK1`   | `Image.HEART`         | `Image.RABBIT`         |
| `Image.CLOCK2`   | `Image.HEART_SMALL`   | `Image.COW`            |
| `Image.CLOCK3`   | `Image.HAPPY`         | `Image.MUSIC_CROTCHET` |
| `Image.CLOCK4`   | `Image.SMILE`         | `Image.MUSIC_QUAVER`   |
| `Image.CLOCK5`   | `Image.SAD`           | `Image.MUSIC_QUAVERS`  |
| `Image.CLOCK6`   | `Image.CONFUSED`      | `Image.PITCHFORK`      |
| `Image.CLOCK7`   | `Image.ANGRY`         | `Image.PACMAN`         |
| `Image.CLOCK8`   | `Image.ASLEEP`        | `Image.TARGET`         |
| `Image.CLOCK9`   | `Image.SURPRISED`     | `Image.TSHIRT`         |
| `Image.CLOCK10`  | `Image.SILLY`         | `Image.ROLLERSKATE`    |
| `Image.CLOCK11`  | `Image.FABULOUS`      | `Image.DUCK`           |
| `Image.CLOCK12`  | `Image.MEH`           | `Image.HOUSE`          |
| `Image.ARROW_N`  | `Image.YES`           | `Image.TORTOISE`       |
| `Image.ARROW_NE` | `Image.NO`            | `Image.BUTTERFLY`      |
| `Image.ARROW_E`  | `Image.TRIANGLE`      | `Image.STICKFIGURE`    |
| `Image.ARROW_SE` | `Image.TRIANGLE_LEFT` | `Image.GHOST`          |
| `Image.ARROW_S`  | `Image.CHESSBOARD`    | `Image.SWORD`          |
| `Image.ARROW_SW` | `Image.DIAMOND`       | `Image.GIRAFFE`        |
| `Image.ARROW_W`  | `Image.DIAMOND_SMALL` | `Image.SKULL`          |
| `Image.ARROW_NW` | `Image.SQUARE`        | `Image.UMBRELLA`       |
| `Image.XMAS`     | `Image.SQUARE_SMALL`  | `Image.SNAKE`          |

![Liste aller enthaltenen Bilder ©](./built_in_images.png)

## Bilderlisten

Ausserdem sind zwei Listen von Bildern vorhanden, welche alle Uhren und Pfeile zusammenfassen:

- `Image.ALL_CLOCKS`
- `Image.ALL_ARROWS`

## Eigene Bilder erstellen

~~~ python
microbit.Image(<mark>text</mark>)
~~~
erstellt ein `Image` aus der Zeichenkette `text`. Dabei besteht die Zeichenkette aus den Ziffern `0` bis `9`, welche die Helligkeit für je ein Pixel angeben. Die Ziffern werden zeilenweise von oben nach unten in der Zeichenkette angegeben. Die einzelnen Zeilen werden durch einen Doppelpunkt `:` getrennt.

Das folgende Beispiel zeigt ein X auf dem Display an:
``` python
from microbit import *

bild = Image("90009:09090:00900:09090:90009")
display.show(bild)
```

~~~ python
bild = microbit.Image(<mark>breite</mark>, <mark>hoehe</mark>)
~~~
erstellt ein `Image`, welches `breite` Pixel breit und `hoehe` Pixel hoch ist.

~~~ python
<mark>bild</mark>.width()
~~~
liefert die Breite des Bildes in Pixel zurück.

~~~ python
<mark>bild</mark>.height()
~~~
liefert die Höhe des Bildes in Pixel zurück.

## Pixel manipulieren

Mit den folgenden Anweisungen können einzelne Pixel eines Bildes manipuliert werden.

~~~ python
<mark>bild</mark>.fill(<mark>helligkeit</mark>)
~~~
setzt jedes Pixel im Bild auf die angegebene Helligkeit. Für `helligkeit` wird eine Zahl zwischen `0` (dunkel) und `9` (hell) angegeben.

~~~ python
<mark>bild</mark>.set_pixel(<mark>x</mark>, <mark>y</mark>, <mark>helligkeit</mark>)
~~~
legt die Helligkeit des Pixels an den Koordinaten `x` und `y` des Bildes fest. Für `helligkeit` wird eine Zahl zwischen `0` (dunkel) und `9` (hell) angegeben.

~~~ python
<mark>bild</mark>.get_pixel(<mark>x</mark>, <mark>y</mark>)
~~~
liefert die aktuelle Helligkeit des Pixels an den Koordinaten `x` und `y` des Bildes zurück. Der zurückgelieferte Wert liegt zwischen `0` (dunkel) und `9` (hell).

## Abgeänderte Bilder erstellen

Mit den folgenden Anweisungen können abgeänderte Kopien von Bildern erstellt werden.

~~~ python
<mark>bild</mark>.copy()
~~~
liefert eine Kopie von `bild` zurück.

~~~ python
<mark>bild</mark>.invert()
~~~
liefert eine invertierte Kopie von `bild` zurück.

~~~ python
<mark>bild</mark>.shift_left(<mark>n</mark>)
~~~
liefert eine Kopie von `bild` zurück, wobei das Bild um `n` Pixel nach links verschoben ist.

~~~ python
<mark>bild</mark>.shift_right(<mark>n</mark>)
~~~
liefert eine Kopie von `bild` zurück, wobei das Bild um `n` Pixel nach rechts verschoben ist.

~~~ python
<mark>bild</mark>.shift_up(<mark>n</mark>)
~~~
liefert eine Kopie von `bild` zurück, wobei das Bild um `n` Pixel nach oben verschoben ist.

~~~ python
<mark>bild</mark>.shift_down(<mark>n</mark>)
~~~
liefert eine Kopie von `bild` zurück, wobei das Bild um `n` Pixel nach unten verschoben ist.

~~~ python
<mark>bild</mark>.crop(<mark>x</mark>, <mark>y</mark>, <mark>w</mark>, <mark>h</mark>)
~~~
liefert eine Kopie von `bild` zurück, wobei die Kopie nur `w` Pixel breit und `h` Pixel hoch ist. Das Pixel an der Koordinate `x`, `y` wird zum Pixel oben links in der Kopie.

~~~ python
<mark>bild1</mark> + <mark>bild2</mark>
~~~
liefert ein neues Bild zurück, wobei die Helligkeit von jedem Pixel von `bild1` und `bild2` addiert wird.

~~~ python
<mark>bild</mark> * <mark>n</mark>
~~~
liefert eine Kopie von `bild` zurück, wobei die Helligkeit von jedem Pixel mit `n` multipliziert wird.
