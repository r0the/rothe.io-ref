# Firmware-Update
---

## Firmware-Version überprüfen

Um die Firmware-Version zu überprüfen, muss der micro:bit via USB mit einem Computer verbunden werden. Am Computer meldet sich der micro:bit als Laufwerk mit dem Namen _MICROBIT_ an. Es enthält die Datei _DETAILS.TXT_, die so aussieht:

```
# DAPLink Firmware - see https://mbed.com/daplink
Unique ID: 9900000047044e4500650004000000400000000097969901
HIC ID: 97969901
Auto Reset: 1
Automation allowed: 0
Overflow detection: 1
Daplink Mode: Interface
Interface Version: 0249
Git SHA: 9c5fd81e6545d00b7f7c21ca9d8577dbd6a5fed2
Local Mods: 0
USB Interfaces: MSD, CDC, HID, WebUSB
Interface CRC: 0xcdb7b2a3
Remount count: 0
URL: https://microbit.org/device/?id=9900&v=0249
```

Der Eintrag _Interface Version_ bezeichnet die Versionsnummer der Firmware.

## Firmware herunterladen

Die neueste Firmware kann direkt hier herunterladen werden:

* [:download: micro:bit-Firmware Version 249](./0249_microbit_firmware.hex)

Die offizielle Download-Seite für die micro:bit-Firmware ist hier:

* [:link: micro:bit Firmware](https://microbit.org/get-started/user-guide/firmware/)

## Firmware installieren

Um die Firmware zu aktualisieren, muss der micro:bit im *maintenance mode* am Computer angeschlossen werden. Dazu drückt man die _Reset_-Taste, währen man den micro:bit per USB am Computer anschliessen. Im *maintenance mode* erscheint der micro:bit als Laufwerk mit dem Namen _MAINTENANCE_.

Die Firmaware wird installiert, indem die entsprechende Datei in das _MAINTENANCE_-Laufwerk kopiert wird.
