# Musik
---

Der micro:bit kann auch Musik abspielen. Dazu muss das Modul `music` importiert werden:

``` python
import music
```

## Lieder abspielen

~~~ python
music.play(<mark>lied</mark>)
~~~
spielt das angegebene Lied ab. Die folgenden Lieder sind vordefiniert:

|                     |                   |                    |
|:------------------- |:----------------- | ------------------ |
| `music.DADADADUM`   | `music.BLUES`     | `music.CHASE`      |
| `music.ENTERTAINER` | `music.BIRTHDAY`  | `music.BA_DING`    |
| `music.PRELUDE`     | `music.WEDDING`   | `music.WAWAWAWAA`  |
| `music.ODE`         | `music.FUNERAL`   | `music.JUMP_UP`    |
| `music.NYAN`        | `music.PUNCHLINE` | `music.JUMP_DOWN`  |
| `music.RINGTONE`    | `music.PYTHON`    | `music.POWER_UP`   |
| `music.FUNK`        | `music.BADDY`     | `music.POWER_DOWN` |

Um also das vordefinierte Lied `music.PYTHON` abzuspielen, schreibt man:

``` python
import music

music.play(music.PYTHON)
```


## Lieder definieren

Es können auch eigene Lieder als Liste von Noten definiert werden:

``` python
frere_jaques = [
  "C4:4", "D4:4", "E4:4", "C4:4", "C4:4", "D4:4", "E4:4",
  "C4:4", "E4:4", "F4:4", "G4:8", "E4:4", "F4:4", "G4:8"
]
```

Jede Note setzt sich aus der Tonbezeichnung, Oktave, und der Länge zusammen. `"C4:2"` bedeutet also ein c in der vierten Oktave für eine Länge von 2.

- **Tonbezeichnung:** Die Tonbezeichnungen `A` bis `G` können verwendet werden. Das h wird wie im englischen Sprachraum üblich als `B` geschrieben. Halbtöne werden mit einem Hashtag markiert: Ein cis wird als `C#` geschrieben, ein
- **Oktave:** Es können die Oktaven 0 bis 8 verwendet werden.
- **Länge:** Damit ist wirklich die Länge gemeint. Eine Note der Länge 4 dauert doppelt so lange wie eine Note der Länge 2.



``` python
import music                       # Musik-Modul importieren

music.pitch(880, 6)                # Ton der Frequenz 880 Hz abspielen für 6 ms abspielen

# eine Tonfolge:

tune = ["C4:2", "D", "E", "C", "C", "D", "E", "C", "E", "F", "G:4",
        "E:2", "F", "G:4"]
music.play(tune)


#--------------------------------------------------------------------------
# eine an- und abschwellende Sirene:

while not microbit.button_a.is_pressed():
    for freq in range(440, 1760, 16):
        music.pitch(freq, 6)
    for freq in range(1760, 440, -16):
        music.pitch(freq, 6)
