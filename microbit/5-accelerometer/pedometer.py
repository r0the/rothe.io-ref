from microbit import *

def zeige_schritte():
    display.scroll(str(schritte), delay=150, wait=False, loop=True)

schritte = 0
zeige_schritte()
while True:
    if accelerometer.was_gesture('shake'):
        schritte = schritte + 1
        zeige_schritte()
    if button_a.was_pressed():
        schritte = 0
        zeige_schritte()
