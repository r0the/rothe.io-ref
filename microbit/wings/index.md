# Fernsteuerung
---

Der micro:bit kann vom PC aus ferngesteuert werden. Dazu sind drei Dinge nötig:

1. Auf dem micro:bit muss das folgende Programm installiert werden:

    [microbit_wings_v2.hex](./microbit_wings_v2.hex)

2. In Thonny muss die Bibliothek `microbit_wings` installiert werden.


## UART-Schnittstelle

Die Bibliothek `microbit_wings` kommuniziert mit dem micro:bit über eine UART-Schnittstelle. Dies ist ein Protokoll, welches typischerweise für die Kommunikation mit Ein- und Ausgabegeräten wie Maus oder Tastatur verwendet wird.

Jedes Gerät erhält vom Betriebssystem einen Namen. Um einen micro:bit ansteuern zu können, muss der richtige Name herausgefunden werden. Mit dem Befehl `get_serial_ports` können die vorhandenen Geräte aufgelistet werden:

``` python
from microbit_wings import

anschluesse = get_serial_ports()
print(anschluesse)
```

### Windows

Die Ausgabe von diesem Programm sieht auf Windows beispielsweise so aus:

```
['COM1', 'COM2', 'COM10']
```


### macOS

Auf macOS sieht die Ausgabe etwa so aus:

```
['/dev/tty.Bluetooth-Incoming-Port', '/dev/tty.usbmodem141502']
```


``` python
from microbit_wings import

microbit.config.write_uart_port('usbmodem141502')
microbit.config.read()  # lese die Konfigurationseinstellungen
```


## Kommunikation starten

~~~ python
microbit.open()
~~~
öffnet die Verbindung mit dem micro:bit

## Bilder anzeigen

~~~ python
microbit.leds_update(b)
~~~
zeigt das Bild _b_ auf dem micro:bit an. Für _b_ können folgende Werte angegeben werden:

~~~ python
microbit.leds_clear_all_update()
~~~
löscht das Display

~~~ python
sleep(s)
~~~
hält die Ausführung des Programms für _s_ Sekunden an.

## Leuchtdioden kontrollieren

~~~
microbit.leds_clear_all()
~~~

~~~
microbit.leds[y][x] = True/False
~~~

~~~
microbit.update()
~~~
