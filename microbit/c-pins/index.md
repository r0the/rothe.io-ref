# Pins
---

| Objekt          | Funktion                                   | zusätzliche Funktion |
|:--------------- |:------------------------------------------ | --------------------:|
| `accelerometer` | [Beschleunigungssensor][11]                |                      |
| `button_a`      | [linke Taste][12]                          |                      |
| `button_b`      | [rechte Taste][12]                         |                      |
| `compass`       | [Magnetometer (Kompass)][13]               |                      |
| `display`       | [5x5 LED-Matrix][14]                       |                      |
| `pin0`          | [digitaler][15] / [analoger][16] Anschluss |          Touch-Taste |
| `pin1`          | [digitaler][15] / [analoger][16] Anschluss |          Touch-Taste |
| `pin2`          | [digitaler][15] / [analoger][16] Anschluss |          Touch-Taste |
| `pin3`          | [digitaler][15] / [analoger][16] Anschluss |              Display |
| `pin4`          | [digitaler][15] / [analoger][16] Anschluss |              Display |
| `pin6`          | [digitaler][15] Anschluss                  |              Display |
| `pin7`          | [digitaler][15] Anschluss                  |              Display |
| `pin8`          | [digitaler][15] Anschluss                  |                      |
| `pin9`          | [digitaler][15] Anschluss                  |              Display |
| `pin10`         | [digitaler][15] / [analoger][16] Anschluss |              Display |
| `pin12`         | [digitaler][15] Anschluss                  |                      |
| `pin13`         | [digitaler][15] Anschluss                  |             SPI MOSI |
| `pin14`         | [digitaler][15] Anschluss                  |             SPI MISO |
| `pin15`         | [digitaler][15] Anschluss                  |              SPI SCK |
| `pin16`         | [digitaler][15] Anschluss                  |                      |
| `pin19`         | [digitaler][15] Anschluss                  |              I2C SCL |
| `pin20`         | [digitaler][15] Anschluss                  |              I2C SDA |
| `radio`         | [Funk][17]                                 |                      |

## Links