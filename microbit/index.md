# :mdi-chip: micro:bit
---

![micro:bit ©](./microbit-red-heart.png)



## Python auf dem micro:bit

Um auf Hardware des micro:bit zugreifen zu können, muss erst das Modul `microbit` in das Python-Skript importiert werden. Dies geschieht mit der Anweisung

``` python
from microbit import *
```

## Referenz

* [1 Aufbau](?page=1-hardware/)
* [2 Display](?page=2-display/)
* [3 Bilder](?page=3-image/)
* [4 Tasten](?page=4-buttons/)
* [5 Beschleunigungssensor](?page=5-accelerometer/)
* [6 Kompass](?page=6-compass/)
* [7 digitale Ein-/Ausgabe](?page=7-digital-io/)
* [8 analoge Ein-/Ausgabe](?page=8-analog-io/)
* [9 Funk](?page=9-radio/)
* [A Musik](?page=a-music/)
* [B Weiteres](?page=b-microbit/)
* [C Anschlüsse](?page=c-pins/)

## Links

* [:link: BCC micro:bit Dokumentation](https://microbit-micropython.readthedocs.io/en/latest/)
* [:link: 101 Computing: Projektideen](https://www.101computing.net/category/bbc-microbit/)

[1]:
[2]:

[11]: ?page=5-accelerometer
[12]: ?page=4-buttons
[13]: ?page=6-compass
[14]: ?page=2-display
[15]: ?page=7-digital-io
[16]: ?page=8-analog-io
[17]: ?page=9-radio
